//
// Created by yakov on 3/24/19.
//

#ifndef DISTRIBUTED_ALGO_1_MESSAGE_H
#define DISTRIBUTED_ALGO_1_MESSAGE_H

#include "pa1_starter_code/ipc.h"
#include "process.h"

Message *create_message(MessageType type, struct process *process);

MessageHeader *create_message_header(MessageType type);

#endif //DISTRIBUTED_ALGO_1_MESSAGE_H
