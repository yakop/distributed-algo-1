//
// Created by yakov on 3/9/19.
//

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "process.h"
#include "message.h"

#define log(...) fprintf(log_file, __VA_ARGS__)

FILE *log_file;

size_t process_count;

static void check_if_all_started(struct process *process);

static void check_if_all_done(struct process *process);

static bool check_and_update_state(struct process *process, enum state desired_state, enum state become_state);

static void notify_all(struct process *process, MessageType type);

struct process *init_process(int process_id) {
    struct process *const process = malloc(sizeof(struct process));
    process->is_parent = process_id == 0;
    process->state = P_STARTED;
    process->id = process_id;
    process->pid = getpid();
    process->ppid = getppid();

    return process;
}

void run(struct process *process) {
    log(log_started_fmt, process->id, process->pid, process->ppid);

    if (!process->is_parent) {
        notify_all(process, STARTED);
    }

    while (process->state != P_DONE) {
        switch (process->state) {
            case P_NEW:
                break;
            case P_STARTED:
                check_if_all_started(process);
                break;
            case P_WAITING_FOR_END:
                check_if_all_done(process);
            case P_DONE:
                break;
        }
    }
}

void check_if_all_started(struct process *process) {
    if (check_and_update_state(process, P_STARTED, P_WAITING_FOR_END)) {
        log(log_received_all_started_fmt, process->id);
        log(log_done_fmt, process->id);

        if (!process->is_parent) {
            notify_all(process, DONE);
        }
    }
}

void check_if_all_done(struct process *process) {
    if (check_and_update_state(process, P_DONE, P_DONE)) {
        log(log_received_all_done_fmt, process->id);
    }
}

bool check_and_update_state(struct process *process, enum state desired_state, enum state become_state) {
    bool all_in_right_state = true;
    local_id first_not_in_right_state = 1;

    for (int i = 1; i < process_count; ++i) {
        if (i != process->id && process->processes[i].state != desired_state) {
            all_in_right_state = false;
            first_not_in_right_state = (local_id) i;
            break;
        }
    }

    if (all_in_right_state) {
        process->state = become_state;
    } else {
        Message *message = malloc(sizeof(Message));
        receive(process, first_not_in_right_state, message);
        free(message);
    }

    return all_in_right_state;
}

void notify_all(struct process *process, MessageType type) {
    Message *const message = create_message(type, process);
    send_multicast(process, message);
    free(message);
}

int send(void *self, local_id dst, const Message *msg) {
    const struct process *process = (const struct process *) self;

    int fd_to_send = process->processes[dst].fd_to_write;

    if (write(fd_to_send, msg, sizeof(Message)) < 0) {
        return 1;
    }

    return 0;
}

int send_multicast(void *self, const Message *msg) {
    const struct process *process = (const struct process *) self;
    int status = 0;

    for (int8_t i = 0; i < process_count; ++i) {
        if (i != process->id) {
            status |= send(self, i, msg);
        }
    }

    return status;
}

int receive(void *self, local_id from, Message *msg) {
    struct process *this_process = (struct process *) self;
    struct process_entry *process_to_receive = &this_process->processes[from];

    if (read(process_to_receive->fd_to_read, msg, sizeof(MessageHeader)) < 0) {
        return 1;
    }

    switch (msg->s_header.s_type) {
        case STARTED:
            process_to_receive->state = P_STARTED;
            break;
        case DONE:
            process_to_receive->state = P_DONE;
            break;
        default:
            break;
    }

    return 0;
}
