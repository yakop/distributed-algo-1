//
// Created by yakov on 3/9/19.
//

#ifndef DISTRIBUTED_ALGO_1_PROCESS_H
#define DISTRIBUTED_ALGO_1_PROCESS_H

#include "pa1_starter_code/ipc.h"
#include "pa1_starter_code/pa1.h"
#include <stdbool.h>

enum state {
    P_NEW,
    P_STARTED,
    P_WAITING_FOR_END,
    P_DONE
};

struct process_entry {
    enum state state;
    int fd_to_read;
    int fd_to_write;
};

struct process {
    bool is_parent;
    struct process_entry processes[MAX_PROCESS_ID + 1];
    enum state state;
    int id;
    int pid;
    int ppid;
};

struct process *init_process(int process_id);

void run(struct process *process);

#endif //DISTRIBUTED_ALGO_1_PROCESS_H
