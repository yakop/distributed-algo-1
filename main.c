#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "process.h"
#include "pa1_starter_code/ipc.h"
#include "pa1_starter_code/common.h"

struct edge {
    int fd_to_write;
    int fd_to_read;
};

struct edge *edges[MAX_PROCESS_ID + 1][MAX_PROCESS_ID + 1];

extern size_t process_count;

extern FILE *log_file;

void prepare_log_file();

void build_fd_graph();

void create_edge(int from, int to);

void run_child_process(int i);

void run_parent_process();

struct process *prepare_process(int process_id);

void close_external_fds(int process_id);

int main(int argc, char **argv) {
    prepare_log_file();
    process_count = (size_t) atoi(argv[2]) + 1;

    build_fd_graph();

    for (int i = 0; i < process_count - 1; ++i) {
        run_child_process(i);
    }

    run_parent_process();

    int status = 0;
    while (wait(&status) > 0);

    return 0;
}

void prepare_log_file() {
    unlink(events_log);
    log_file = fopen(events_log, "a+");
}

void build_fd_graph() {
    for (int i = 0; i < process_count; ++i) {
        for (int j = 0; j < process_count; ++j) {
            if (i != j) {
                create_edge(i, j);
            }
        }
    }
}

void create_edge(int from, int to) {
    int fds[2];
    pipe(fds);
    edges[from][to] = malloc(sizeof(struct edge));
    edges[from][to]->fd_to_write = fds[1];
    edges[from][to]->fd_to_read = fds[0];
}

void run_parent_process() {
    int process_id = 0;

    close_external_fds(0);

    struct process *process = prepare_process(process_id);
    run(process);
}

void run_child_process(int i) {
    int pid = fork();

    if (pid == 0) {
        struct process *process = prepare_process(i + 1);
        run(process);
        free(process);
        exit(0);
    }
}

struct process *prepare_process(int process_id) {
    struct process *process = init_process(process_id);

    for (int i = 0; i < process_count; ++i) {
        if (i != process_id) {
            struct edge *edge_for_current_to_read = edges[process_id][i];
            struct edge *edge_for_current_to_write = edges[i][process_id];

            process->processes[i].state = P_NEW;
            process->processes[i].fd_to_write = edge_for_current_to_write->fd_to_write;
            process->processes[i].fd_to_read = edge_for_current_to_read->fd_to_read;
        }
    }

    if (!process->is_parent) {
        close_external_fds(process_id);
    }

    return process;
}

void close_external_fds(int process_id) {
    for (int i = 0; i < process_count; ++i) {
        for (int j = 0; j < process_count; ++j) {
            for (int j = 0; j < process_count; ++j) {
                if (i != j && j != process_id) {
                    close(edges[i][j]->fd_to_write);
                }

                if (i != j && i != process_id) {
                    close(edges[i][j]->fd_to_read);
                }
            }
        }
    }
}
