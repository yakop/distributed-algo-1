//
// Created by yakov and julia on 3/24/19.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "message.h"
#define set_payload(...) sprintf(msg->s_payload, __VA_ARGS__)

Message *create_message(MessageType type, struct process *process) {
    Message *msg = malloc(sizeof(Message));
    msg->s_header = *create_message_header(type);
    msg->s_header.s_type = type;

    switch (type) {
        case STARTED:
            set_payload(log_started_fmt, process->id, process->pid, process->ppid);
            break;
        case DONE:
            set_payload(log_done_fmt, process->id);
            break;
        default:
            break;
    }

    msg->s_header.s_payload_len = (uint16_t) strlen(msg->s_payload);

    return msg;
}

MessageHeader *create_message_header(MessageType type) {
    MessageHeader *msg_header = malloc(sizeof(MessageHeader));
    msg_header->s_magic = MESSAGE_MAGIC;
    msg_header->s_type = type;

    return msg_header;
}
